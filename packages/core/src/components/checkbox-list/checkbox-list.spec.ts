import { newSpecPage } from '@stencil/core/testing';
import { CheckboxList } from './checkbox-list';
 
describe('kp-checkbox-list', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [CheckboxList],
      html: '<kp-checkbox-list></kp-checkbox-list>',
    });
    expect(root).toEqualHtml(`
      <kp-checkbox-list>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </kp-checkbox-list>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [CheckboxList],
      html: `<kp-checkbox-list first="Stencil" last="'Don't call me a framework' JS"></kp-checkbox-list>`,
    });
    expect(root).toEqualHtml(`
      <kp-checkbox-list first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </kp-checkbox-list>
    `);
  });
});
