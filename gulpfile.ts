import { parallel, series, task } from 'gulp';
import './script/gulp/task/build';
import './script/gulp/task/watch';
import './script/gulp/task/start';

// build core task
task('build:core',
  series(
    'clean:core.publish',
    'build:core.publish',
    'copy:core.publish'
  )
);

// build package/vue task
task('build:vue',
  series(
    'clean:vue.publish',
    'update:vue.node_modules.keep-ui-core',
    'compile:vue.publish',
    'build:vue.publish',
    'copy:vue.publish',
  )
);

task('start:vue', parallel(
  'watch:package/vue',
  'watch:core/src/css',
  'start:core',
  'start:site/vue'
));
