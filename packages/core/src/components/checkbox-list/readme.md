# my-component



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description     | Type               | Default     |
| -------- | --------- | --------------- | ------------------ | ----------- |
| `first`  | `first`   | The first name  | `string`           | `undefined` |
| `last`   | `last`    | The last name   | `string`           | `undefined` |
| `middle` | `middle`  | The middle name | `string`           | `undefined` |
| `value`  | `value`   |                 | `number \| string` | `23`        |


## Events

| Event      | Description | Type                  |
| ---------- | ----------- | --------------------- |
| `kpChange` |             | `CustomEvent<object>` |


## Dependencies

### Depends on

- [kp-checkbox](../checkbox)

### Graph
```mermaid
graph TD;
  kp-checkbox-list --> kp-checkbox
  style kp-checkbox-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
