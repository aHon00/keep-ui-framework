import { task } from "gulp";
import * as child_process from "child_process";
import { packagePath } from "../util/task-helper";
const execa = require('execa');
task('start:site/vue', async (done) => {
  // try {
  //   await execa('npm', ['run', 'start'], { cwd: packagePath('site/vue') }).stdout.pipe(process.stdout);
  //   done();
  // } catch (error) {
  //   console.log(error);
  //   done()
  // }
  const childProcess = child_process.spawn(packagePath('site/vue/node_modules/.bin/vue-cli-service.cmd'), ['serve', '--open'], {
    cwd: packagePath('site/vue'),
    stdio: 'inherit'
  });
  childProcess.on('close', (code: number) => {
    code != 0 ? done(new Error(`Process failed with code ${code}`)) : done();
  });
});

task('start:core', async(done) => {
  await execa('npm', ['run', 'start'], { cwd: packagePath('packages/core') }).stdout.pipe(process.stdout);
  // const childProcess = child_process.spawn(packagePath('packages/core/node_modules/.bin/stencil.cmd'), ['build', '--watch', '--serve', '--no-open'], {
  //   cwd: packagePath('packages/core'),
  //   stdio: 'inherit'
  // });
  // childProcess.on('close', (code: number) => {
  //   code != 0 ? done(new Error(`Process failed with code ${code}`)) : done();
  // });
});