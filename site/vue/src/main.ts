
import { KeepUiVue } from "keep-ui-vue";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import 'keep-ui-core/css/index.css'
import '@/style/index.less';
createApp(App).use(router).use(KeepUiVue as any).mount("#app");
