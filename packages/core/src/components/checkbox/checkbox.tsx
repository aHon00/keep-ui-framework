import { Component, Host, h, EventEmitter, Event, Prop, Watch, Element } from '@stencil/core';

@Component({
  tag: 'kp-checkbox',
  styleUrl: 'checkbox.less',
  shadow: true,
})
export class KpCheckbox {
  private inputId = `kp-cb-${checkboxIds++}`;

  focusEl?: HTMLInputElement;

  @Element() el!: HTMLElement;

  @Event() kpChange: EventEmitter<boolean>;

  @Prop() name: string = this.inputId;

  @Prop({ mutable: true }) checked = false;

  @Prop() disabled = false;

  @Prop() value = 'on';
  /**
   * Emitted when the checkbox has focus.
   */
  @Event() kpFocus!: EventEmitter<void>;

  /**
   * Emitted when the checkbox loses focus.
   */
  @Event() kpBlur!: EventEmitter<void>;

  @Prop({ mutable: true }) indeterminate = false;

  @Watch('checked')
  valueChange() {
    this.kpChange.emit(this.checked);
  }
  private onClick = (ev: any) => {
    ev.preventDefault();
    this.checked = !this.checked;
  };

  private onFocus = () => {
    this.kpFocus.emit();
  };

  private onBlur = () => {
    this.kpBlur.emit();
  };
  render() {
    const { checked, disabled, inputId, indeterminate } = this;
    if (this.focusEl) {
      this.focusEl.value = checked ? 'on' : '';
    }
    return (
      <Host onClick={!disabled && this.onClick}>
        <label class={{ 'kp-checkbox-wrapper': !disabled && true, 'kp-checkbox-wrapper-disabled': disabled }}>
          <span
            class={{
              'kp-checkbox': true,
              'kp-checkbox-checked': checked,
              'kp-checkbox-indeterminate': checked ? false : indeterminate,
            }}
          >
            <input
              class="kp-checkbox-input"
              type="checkbox"
              aria-checked={`${checked}`}
              disabled={disabled}
              id={inputId}
              checked={checked}
              onFocus={() => this.onFocus()}
              onBlur={() => this.onBlur()}
              ref={focusEl => (this.focusEl = focusEl)}
            />
            <span class="kp-checkbox-inner"></span>
          </span>
          <span class="kp-checkbox-text">
            <slot></slot>
          </span>
        </label>
      </Host>
    );
  }
}
let checkboxIds = 0;
