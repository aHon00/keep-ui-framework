# kp-button



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description | Type                              | Default     |
| ------------ | ------------- | ----------- | --------------------------------- | ----------- |
| `KpType`     | `kp-type`     |             | `"primary"`                       | `'primary'` |
| `kpDisabled` | `kp-disabled` |             | `boolean`                         | `false`     |
| `kpSize`     | `kp-size`     |             | `"default" \| "large" \| "small"` | `'default'` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
