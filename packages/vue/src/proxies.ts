import { defineContainer } from './vue-component-lib/utils';

import type { JSX } from 'keep-ui-core';

export const kpButton = /*@__PURE__*/ defineContainer<JSX.KpButton>('kp-button',
  [
    'checked',
    'disabled',
    'indeterminate',
    'kpChange',
  ],
  {
    modelUpdateEvent: [
      "v-kpChange",
      "v-kp-change"
    ],
    externalModelUpdateEvent: "kpChange"
  });

export const KpCheckbox = /*@__PURE__*/ defineContainer<JSX.KpCheckbox>('kp-checkbox',
  [
    'name',
    
    'checked',
    'disabled',
    'value',
    'indeterminate',
    'kpChange',
    'kpFocus',
    'kpBlur'
  ],
  {
    modelProp: 'checked',
    modelUpdateEvent: [
      "v-kpChange",
      "v-kp-change"
    ],
    externalModelUpdateEvent: "kpChange"
  });


export const KpCheckboxList = /*@__PURE__*/ defineContainer<JSX.KpCheckboxList>('kp-checkbox-list',
  [
    'first',
    'middle',
    'value',
    'kpChange'
  ],
  {
    modelProp: 'value',
    modelUpdateEvent: [
      "v-kpChange",
      "v-kp-change"
    ],
    externalModelUpdateEvent: "kpChange"
  });