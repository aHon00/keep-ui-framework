import { task } from 'gulp';
import { cleanTask, packagePath } from '../util/task-helper';
task(
  'clean:core.publish', cleanTask('publish', { cwd: packagePath('core') })
);
task(
  'clean:vue.publish', cleanTask('publish', { cwd: packagePath('packages/vue') })
);