import { defineComponent, h, ref, VNode } from "vue";

const UPDATE_VALUE_EVENT = 'update:modelValue';
const MODEL_VALUE = 'modelValue';
export interface ComponentOptions {
  /**
   * 暴露出需要双向绑定的属性
   */
  modelProp?: string;
  /**
   * 暴露对应双向绑定的变化事件
   */
  modelUpdateEvent?: string | string[];
  externalModelUpdateEvent?: string;
}
export interface InputProps extends Object {
  modelValue: string | boolean;
}
/**
* 创建一个回调去定义一个包裹web components的vue component容器
*
* @prop name - The component tag name (kp-checkbox-list)
* @prop componentProps - An array of properties on the
* component.
* @prop componentOptions - An object that defines additional
* options for the component such as router or v-model
* integrations.
*/
export const defineContainer = <Props>(name: string, componentProps: string[] = [], componentOptions: ComponentOptions = {}) => {
  const { modelProp, modelUpdateEvent, externalModelUpdateEvent } = componentOptions;
  /**
   * Create a Vue component wrapper around a Web Component.
   * Note: The `props` here are not all properties on a component.
   * They refer to whatever properties are set on an instance of a component.
   */
  const Container = defineComponent<Props & InputProps>((props, { slots, emit }) => {
    let modelPropValue = modelProp && (props as any)[modelProp];
    const containerRef = ref<HTMLElement>();
    const onVnodeBeforeMount = (vnode: VNode) => {
      const eventsNames = Array.isArray(modelUpdateEvent) ? modelUpdateEvent : [modelUpdateEvent];
      eventsNames?.forEach((eventName: string | undefined) => {
        if (vnode.el) {
          vnode.el.addEventListener(eventName?.toLowerCase(), (e: Event) => {
            modelPropValue = modelProp && (e?.target as any)[modelProp];
            emit(UPDATE_VALUE_EVENT, modelPropValue);
            if (externalModelUpdateEvent) {
              emit(externalModelUpdateEvent, e);
            }
          });
        }
      });
    }
    return () => {
      let propsToAdd = {
        ...props,
        ref: containerRef,
        onVnodeBeforeMount: (modelUpdateEvent && externalModelUpdateEvent) ? onVnodeBeforeMount : undefined,
      };
      if (modelProp) {
        propsToAdd = {
          ...propsToAdd,
          [modelProp]: props.hasOwnProperty(MODEL_VALUE) && props[MODEL_VALUE] !== undefined ? props.modelValue : modelPropValue
        }
      }
      return h(name, propsToAdd, slots.default && slots.default());
    }
  });
  Container.displayName = name;
  Container.props = [...componentProps];
  if (modelProp) {
    Container.props.push(MODEL_VALUE);
    Container.emits = [UPDATE_VALUE_EVENT, externalModelUpdateEvent];
  }
  return Container;
}