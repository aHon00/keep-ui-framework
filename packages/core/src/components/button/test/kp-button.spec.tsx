import { newSpecPage } from '@stencil/core/testing';
import { KpButton } from '../button';

describe('kp-button', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [KpButton],
      html: `<kp-button></kp-button>`,
    });
    expect(page.root).toEqualHtml(`
      <kp-button>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </kp-button>
    `);
  });
});
