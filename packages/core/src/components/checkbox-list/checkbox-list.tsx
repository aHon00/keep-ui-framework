import { Component, Prop, h, Event, EventEmitter, Watch, Host } from '@stencil/core';

@Component({
  tag: 'kp-checkbox-list',
  styleUrl: 'checkbox-list.less',
  shadow: true,
})
export class CheckboxList {
  focusEl?: HTMLElement;
  /**
   * The first name
   */
  @Prop() first: string;

  /**
   * The middle name
   */
  @Prop() middle: string;

  /**
   * The last name
   */
  @Prop() last: string;

  @Event() kpChange: EventEmitter<object>;
  @Prop({ mutable: true }) value?: string | number | null = 23;
  @Watch('value')
  valueChange() {
    console.log('value');
  }

  componentDidLoad() {
    this.kpChange.emit({ value: 1321654 });
  }

  render() {
    return (
      <Host>
        <kp-checkbox>sd</kp-checkbox>
      </Host>
    );
  }
}
