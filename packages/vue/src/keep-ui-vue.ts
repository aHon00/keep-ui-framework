import { Plugin } from 'vue';
import { applyPolyfills, defineCustomElements } from 'keep-ui-core/loader';

const toLowerCase = (eventName: string) => eventName === 'kpChange' ? 'v-kpchange' : eventName.toLowerCase();
const toKebabCase = (eventName: string) => eventName === 'kpChange' ? 'v-kp-change' : eventName.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase();
const getHelperFunctions = (needsKebabCase: boolean = true) => {
  const conversionFn = (needsKebabCase) ? toKebabCase : toLowerCase;
  return {
      ael: (el: any, eventName: string, cb: any, opts: any) => el.addEventListener(conversionFn(eventName), cb, opts),
      rel: (el: any, eventName: string, cb: any, opts: any) => el.removeEventListener(conversionFn(eventName), cb, opts),
      ce: (eventName: string, opts: any) => new CustomEvent(conversionFn(eventName), opts)
  };
};

export const KeepUiVue: Plugin = {
  async install() {
    if (typeof (window as any) !== 'undefined') {
      const { ael, rel, ce } = getHelperFunctions(false);

      await applyPolyfills();
      await defineCustomElements(window, {
        ce,
        ael,
        rel
      } as any);
    }
  }
};