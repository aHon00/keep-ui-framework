import { newSpecPage } from '@stencil/core/testing';
import { KpCheckbox } from '../checkbox';

describe('kp-checkbox', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [KpCheckbox],
      html: `<kp-checkbox></kp-checkbox>`,
    });
    expect(page.root).toEqualHtml(`
      <kp-checkbox>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </kp-checkbox>
    `);
  });
});
