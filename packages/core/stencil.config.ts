import { Config } from '@stencil/core';
import { less } from '@stencil/less';
// import { vueOutputTarget } from '@stencil/vue-output-target';
// import { packagePath } from '../../script/gulp/util/task-helper';
export const config: Config = {
  namespace: 'keep-ui-core',
  bundles: [
    { components: ['kp-checkbox-list'] }
  ],
  plugins: [
    less()
  ],
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    // 将在../packages/vue/src/proxies.ts生成定义vue组件的代码以及vue-component-lib相关代码
    // vueOutputTarget({
    //   componentCorePackage: 'keep-ui-core',
    //   proxiesFile: packagePath('packages/vue/src/proxies.ts'),
    //   componentModels: [
    //     {
    //       elements: ['kp-checkbox', 'kp-checkbox-list'],
    //       targetAttr: 'value',
    //       // TODO Ionic v6 remove in favor of v-ion-change
    //       event: ['v-kpChange', 'v-kp-change'],
    //       externalEvent: 'kpChange'
    //     }
    //   ],
    // }),
    {
      type: 'dist-custom-elements-bundle',
    },
    {
      type: 'docs-readme',
    }
  ],
};
