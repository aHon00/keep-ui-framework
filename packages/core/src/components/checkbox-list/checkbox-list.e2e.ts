import { newE2EPage } from '@stencil/core/testing';

describe('kp-checkbox-list', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent('<kp-checkbox-list></kp-checkbox-list>');
    const element = await page.find('kp-checkbox-list');
    expect(element).toHaveClass('hydrated');
  });

  it('renders changes to the name data', async () => {
    const page = await newE2EPage();

    await page.setContent('<kp-checkbox-list></kp-checkbox-list>');
    const component = await page.find('kp-checkbox-list');
    const element = await page.find('kp-checkbox-list >>> div');
    expect(element.textContent).toEqual(`Hello, World! I'm `);

    component.setProperty('first', 'James');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James`);

    component.setProperty('last', 'Quincy');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

    component.setProperty('middle', 'Earl');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
  });
});
