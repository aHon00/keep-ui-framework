import { dest, src, task } from "gulp";
import { copy, packagePath } from "../util/task-helper";
const less = require('gulp-less');
const execa = require('execa');
import './clean';

// copy core publish to publish npm
task('copy:core.publish', copy('core', 'core', 'publish'));

// npm run build at core
task('build:core.publish', async function (done) {
  await execa('npm', ['run', 'build'], { cwd: packagePath('core') });
  done();
})

// copy vue publish to publish npm
task('copy:vue.publish', copy('packages/vue', 'packages/vue', 'publish'));

// npm run compile at vue
task('compile:vue.publish', async function (done) {
  await execa('npm', ['run', 'compile'], { cwd: packagePath('packages/vue') });
  done();
})

// npm run build at vue
task('build:vue.publish', async function (done) {
  await execa('npm', ['run', 'build'], { cwd: packagePath('packages/vue') });
  done();
})

// build core less
task('build:core.less', async function (done) {
  src(packagePath('packages/core/src/css/*.less'))
    .pipe(less())
    .pipe(
      dest(packagePath('packages/core/css'))
    )
  done();
})