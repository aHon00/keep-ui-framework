# kp-checkbox



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute       | Description | Type      | Default        |
| --------------- | --------------- | ----------- | --------- | -------------- |
| `checked`       | `checked`       |             | `boolean` | `false`        |
| `disabled`      | `disabled`      |             | `boolean` | `false`        |
| `indeterminate` | `indeterminate` |             | `boolean` | `false`        |
| `name`          | `name`          |             | `string`  | `this.inputId` |
| `value`         | `value`         |             | `string`  | `'on'`         |


## Events

| Event      | Description                            | Type                   |
| ---------- | -------------------------------------- | ---------------------- |
| `kpBlur`   | Emitted when the checkbox loses focus. | `CustomEvent<void>`    |
| `kpChange` |                                        | `CustomEvent<boolean>` |
| `kpFocus`  | Emitted when the checkbox has focus.   | `CustomEvent<void>`    |


## Dependencies

### Used by

 - [kp-checkbox-list](../checkbox-list)

### Graph
```mermaid
graph TD;
  kp-checkbox-list --> kp-checkbox
  style kp-checkbox fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
