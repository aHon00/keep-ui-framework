import { series, task, watch } from "gulp";
import { packagePath, copy } from "../util/task-helper";
const execa = require("execa");

task(
  "update:vue.node_modules.keep-ui-core",
  copy("core", "packages/vue", "node_modules/keep-ui-core")
);
// watch package/vue update
task("watch:package/vue", function (done) {
  watch(
    packagePath("packages/vue/src"),
    async function compilePackageVue(done1) {
      await execa("npm", ["run", "build"], {
        cwd: packagePath("packages/vue"),
      }).stdout.pipe(process.stdout);
      done1();
    }
  );
  done();
});

// watch packages/core/src/css build
task("watch:core/src/css", function (done) {
  watch(
    packagePath("packages/core/src/css"),
    series(
      'build:core.less'
    )
  );
  done();
});
