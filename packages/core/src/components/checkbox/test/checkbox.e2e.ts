import { newE2EPage } from '@stencil/core/testing';

describe('kp-checkbox', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<kp-checkbox></kp-checkbox>');

    const element = await page.find('kp-checkbox');
    expect(element).toHaveClass('hydrated');
  });
});
