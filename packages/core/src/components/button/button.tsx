import { Component, Host, h, Prop } from '@stencil/core';

export type KpSize = 'default' | 'small' | 'large';
export type KpType = 'primary';
@Component({
  tag: 'kp-button',
  styleUrl: 'button.less',
  shadow: true,
})
export class KpButton {
  @Prop() kpDisabled = false;
  @Prop() kpSize: KpSize = 'default';
  @Prop() KpType: KpType = 'primary';
  render() {
    return (
      <Host>
        <button class={{
          'kp-btn': true,
          'kp-btn-sm': this.kpSize === 'small' ,
          'kp-btn-lg': this.kpSize === 'large' ,
          [`kp-btn-${this.KpType}`]: true
        }}
        >
          <slot></slot>
        </button>
      </Host>
    );
  }
}
