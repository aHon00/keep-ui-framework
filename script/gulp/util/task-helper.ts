import { dest, src, TaskFunction } from "gulp";
const gulpClean = require('gulp-clean');
const path = require('path');

const rootDir = path.join(__dirname, '../../../');
export function packagePath(project: string) {
  return path.join(rootDir, project);
}

export function cleanTask(glob: string | string[], options?: any): TaskFunction {
  return () => src(glob, { read: false, allowEmpty: true, ...options },).pipe(gulpClean(null));
}

export function copy(formPath: string, toPath: string, output: string) {
  function executeCopy(done: any) {
    // dist
    src(['dist/**/*.js', 'dist/**/*.ts', 'dist/**/*.json'], {
      cwd: packagePath(formPath),
      allowEmpty: true
    }).pipe(dest(`${output}/dist`, { cwd: packagePath(toPath) }));

    // loader
    src(['loader/**/*.js', 'loader/**/*.ts', 'loader/**/*.json'], {
      cwd: packagePath(formPath),
      allowEmpty: true
    }).pipe(dest(`${output}/loader`, { cwd: packagePath(toPath) }));

    // package.json
    src(['readme.md', 'package.json', 'LICENSE'], {
      cwd: packagePath(formPath),
      allowEmpty: true
    }).pipe(dest(`${output}`, { cwd: packagePath(toPath) }));
    done();
  };
  executeCopy.description = `copy the ${formPath} dist to ${toPath}`;
  return executeCopy;
}

